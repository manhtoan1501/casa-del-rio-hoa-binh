import { Grid } from '@material-ui/core';


const GeographicalLocation = () => {

  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          VỊ TRÍ ĐỊA LÝ VÀ LIÊN KẾT VÙNG
        </h1>
        <p className='content-base'>
          <strong>Casa Del Rio</strong> tọa lạc tại <b><i>VỊ TRÍ SÁNG GIÁ NHẤT</i></b> của TP Hòa Bình, nằm ngay mặt Sông Đà. Dự án Casa Del Rio không những được thừa hưởng hạ tầng và hệ thống giao thông đồng bộ, địa thế phong thủy hữu tình, mà còn hứa hẹn mang đến giá trị đầu tư và sinh lời vượt trội.
        </p>
        <p className='content-base'>
          Gia chủ có thể tận hưởng toàn 3 lớp tiện ích đã hiện hữu bao gồm cả trong và ngoài khu đô thị như Golf, du lịch tâm linh, club bar, bể bơi, hồ điều hòa, vườn nướng BBQ, dịch vụ công cộng được vận hành theo tiêu chuẩn quốc tế.
        </p>
        <p>
          <strong>Casa Del Rio</strong> Hòa Bình có vị trí đắc địa ngay trục giao thông quan trọng – Quốc lộ 6 kết nối nhanh chóng về thủ đô Hà Nội. Dự án không chỉ thừa hưởng cảnh sắc thiên nhiên tươi đẹp và cái nhìn bao trọn vẻ đẹp của tự nhiên, sông nước của nhịp sống còn người. Mà hơn hết, nơi đây mang lại cho không gian nghỉ dưỡng sự mát mẻ quanh năm cùng không khí trong lành.
        </p>
        <img
          src={'https://casaderio-hoabinh.geleximco.org/wp-content/uploads/2022/08/vi-tri-du-an-casa-del-rio.jpg'}
          className='image-location'
        />
      </Grid>
    </Grid>
  );
}

export default GeographicalLocation;
