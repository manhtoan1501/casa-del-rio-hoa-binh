import { Grid } from '@material-ui/core';

const Advertise = () => {

  return (
    <Grid
      container
      alignItems='center'
      justifyContent='space-between'
      spacing={2}
      className='block-location max-width-base'
    >
      <Grid item md={6} xs={12}>
        <h1 className='title-base'>
          CASA DEL RIO HÒA BÌNH - ĐỊA LINH SINH PHÚ QUÝ
        </h1>
        <p className='content-base'>
          <strong> Casa Del Rio</strong> trong tiếng Tây Ban Nha mang nghĩa là “Nhà bên sông”, là tên gọi chính thức của KĐT nghỉ dưỡng bên sông Đà hùng vĩ & thơ mộng. Dự án như khắc họa tựa như những giai điệu từ thiên nhiên và hòa cùng với ly Margarita mang “vị” của những trải nghiệm tại vùng ven đô – nơi cận kề phố thị nhưng lại cách xa chốn xô bồ. Mang trong mình triết lý “lagon”.
          Dự Án Casa Del Rio là chốn tìm về để khởi tạo năng lượng và lấy lại cân bằng với những giây phút hòa mình cùng thiên nhiên, mang lại cảm giác an yên, đủ đầy và viên mãn.
        </p>
      </Grid>
      <Grid item md={6} xs={12}>
        <iframe
          className="elementor-video-advertise"
          frameBorder="0"
          allowFullScreen="1"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          title="Siêu Phẩm Nghỉ Dưỡng Ven Đô"
          width="100%"
          height="360px"
          src="https://www.youtube.com/embed/38OofF3Zkks?controls=1&amp;rel=0&amp;playsinline=0&amp;modestbranding=0&amp;autoplay=0&amp;enablejsapi=1&amp;origin=https%3A%2F%2Fcasaderio-hoabinh.geleximco.org&amp;widgetid=1"
          id="widget2"
          data-gtm-yt-inspected-7="true"
        ></iframe>
      </Grid>
    </Grid>
  );
}

export default Advertise;
