import { Grid } from '@material-ui/core';


const ViewFull = () => {

  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          MẶT BẰNG TỔNG THỂ
        </h1>
        <p className='content-base'>
          Biệt thự <strong>Casa Del Rio</strong> Hòa Bình tráng lệ với những đường nét kiến trúc tinh tế từ Châu Âu. Nét kiến trúc hiện đại ấy như hòa cùng thiên nhiên giúp cho những cư dân của Dự Án Casa Del Rio được tái tạo năng lượng sau những ngày làm việc mệt nhọc và tái khởi tâm hồn khi rời xa chốn phồn hoa xô bồ, náo nhiệt để tận hưởng những giây phút thư giãn tuyệt vời nhất bên gia đình, bạn bè và người thân.
        </p>
        <img
          src={'https://casaderio-hoabinh.geleximco.org/wp-content/uploads/2022/08/brochure-casa-final-view_compressed_page-0020-2048x1024-1.jpg'}
          className='image-location'
        />
      </Grid>
    </Grid>
  );
}

export default ViewFull;
