import React from 'react';
import Link from 'next/link'

const ImageHome= () => {
   
    const headers = [
        {
            title: 'Trang chủ',
            link: '/',
        },
        // {
        //     title: 'Vị trí',
        //     link: '/vi-tri',
        // },
        // {
        //     title: 'Tin tức',
        //     link: '/tin-tuc',
        // },
    ];

    return (
        <div className='header-home-root'>
            {/* <div className='header-container'>
                {headers.map((element, index) => (
                    <Link
                        key={index}
                        href={element.link}
                    >
                        <a>
                            {element.title}
                        </a>
                    </Link>
                ))}
            </div> */}
            <img
                src={'https://w.ladicdn.com/s1440x863/5ea845b95da1a2557f302daa/fe04fa18110fd2518b1e-20220624070217-20220627021354.jpg'}
                className='image-home'
            />
        </div>
    );
}

export default ImageHome;