import { Grid } from '@material-ui/core';
import {Check} from '@material-ui/icons';

const Utilities = () => {

  const utilities = [
    ['Khách sạn 5 sao', 'Phố ẩm thực ven sông', 'Công viên bellville', 'Khu vui chơi trẻ em', 'Quảng trường poseidon'],
    ['Tổ hợp thương mại Central', 'Trường liên cấp quốc tế', 'Tháp đồng hồ', 'Cổng chào casa', 'Vườn nướng bbq'],
    ['Public art', 'Vườn hoa', 'Trung tâm thương mại', 'Chòi cảnh quan', 'Vườn nghệ thuật'],
    ['Club house', 'Quảng trường nhạc nước', 'Vườn thể thao ngoài trời', 'Resort 6 sao', 'Quảng trường lamour']
  ];
  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          TIỆN ÍCH CASA DELRIO HÒA BÌNH
        </h1>
        <p className='content-base'>
          Ngoài vị trí thuận lợi, không khí trong lành, cảnh quan thoáng đãng, khu đô thị Casa Hòa Bình được tích hợp rất nhiều các tiện ích đẳng cấp, nổi bật. Khi mua 1 ngôi Biệt Thự, Liền Kề, ShopHouse Casa Del Rio anh/chị không chỉ sở hữu cho mình 1 bất động sản hiếm có, 1 nơi nghỉ dưỡng cuối tuần rất gần Hà Nội, 1 sản phẩm đầu tư tốt mà còn sở hữu cho mình hàng loạt các tiện ích dịch vụ cao cấp.
        </p>
      </Grid>
      {utilities.map((element, index) => {
        return (
          <Grid item xs={12} sm={6} md={3} key={index}>
            <div className='block-item-utilitie'>
              {element.map((_element, _index) => (
                <p className='content-item-utilitie' key={_index}>
                  <Check /> {_element}
                </p>
              ))}
            </div>
          </Grid>
        );
      })}
    </Grid>
  );
}

export default Utilities;
