import { Grid, Button } from '@material-ui/core';
import Form from './Form';


const Footer = () => {

  const promises = [
    'Đội ngũ chuyên viên tư vấn chuyên nghiệp, trung thực, nhiệt tình',
    'Mọi thủ tục pháp lý khách hàng đều được thực hiện trực tiếp với CĐT',
    'Cung cấp thông tin nhanh chóng, chính xác và nhanh nhất từ chủ đầu tư',
    'Cam kết bán hàng trực tiếp giá CĐT'
  ];

  const projects = [
    'Dự án Ngôi Nhà Mới Quốc Oai ',
    'Dự án New House Xa La',
    'Dự án Louis City Đại Mỗ',
    'Chuỗi nhà hàng quy mô',
  ];

  const inputs = [
    {
      name: 'NAME',
      placeholder: 'nhập tên'
    },
    {
      name: 'EMAIL',
      placeholder: 'nhập email'
    },
    {
      name: 'SDT',
      placeholder: 'nhập số điện thoại'
    }
  ];

  return (
    <Grid container className='footer-root'>
      <Grid item xs={12} md={3}>
        <div className='item-footer'>
          <p className='title-footer'>
            THÔNG TIN LIÊN HỆ
          </p>
          <p className='item-content'>
            PHÒNG KINH DOANH DỰ ÁN
          </p>
          <p className='item-content'>
            Địa chỉ: Xã Trung Minh - TP. Hòa Bình
          </p>
          <p className='item-content'>
            Hotline: 0979 194 717
          </p>
        </div>
      </Grid>
      <Grid item xs={12} md={3}>
        <div className='item-footer'>
          <p className='title-footer'>
            DỰ ÁN TIÊU BIỂU
          </p>
          {projects.map((element, index) => (
            <p className='item-content' key={index}>
              - {element}
            </p>
          ))}
        </div>
      </Grid>
      <Grid item xs={12} md={3}>
        <div className='item-footer'>
          <p className='title-footer'>
            ĐĂNG KÝ NHẬN BẢNG GIÁ
          </p>
          <Form />
        </div>
      </Grid>
      <Grid item xs={12} md={3}>
        <div className='item-footer'>
          <p className='title-footer'>
            CAM KẾT CỦA CHÚNG TÔI
          </p>
          {promises.map((element, index) => (
            <p className='item-content' key={index}>
              - {element}
            </p>
          ))}
        </div>
      </Grid>
    </Grid>
  )
}

export default Footer;
