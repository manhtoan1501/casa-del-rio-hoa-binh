import { Grid } from '@material-ui/core';


const ViewHoaBinh = () => {

  const listImage = [
    { image: 'https://casa-delrio.com/wp-content/uploads/2021/12/casa-delrio.jpg' },
    { image: 'https://casa-delrio.com/wp-content/uploads/2021/12/khach-san-casa-del-rio-2048x1152.jpg' },
    { image: 'https://casa-delrio.com/wp-content/uploads/2021/12/quang-truong-casa-del-rio-2048x1280.jpg' },
    { image: 'https://casa-delrio.com/wp-content/uploads/2021/12/ho-nuoc-casa-del-rio-2048x1152.jpg' },
    { image: 'https://casa-delrio.com/wp-content/uploads/2021/12/screen4-img.png' },
    { image: 'https://casa-delrio.com/wp-content/uploads/2021/12/casa-del-rio-anh-2048x1152.jpg' },
  ];

  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          CHÂU ÂU GIỮA LÒNG HÒA BÌNH
        </h1>
        <p className='content-base'>
          Với mật độ xây dựng cực thấp, chủ đầu tư đã dành 1 phần lớn diện tích dành cho giao thông công cộng, cây xanh và mặt nước, dự án Casa Del Rio là một trong những Khu đô thị sở hữu hệ sinh thái xanh hoàn thiện và quy mô bậc nhất Hòa Bình.
        </p>
      </Grid>
      {listImage.map((element, index) => (
        <Grid xs={12} sm={6} md={4} item key={index}>
          <img src={element.image} className='image-view-location' />
        </Grid>
      ))}
    </Grid>
  )
}

export default ViewHoaBinh;
