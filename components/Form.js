import React from 'react';
import { useState, } from 'react';
import { database } from '../firebaseConfig';
import { collection, addDoc } from 'firebase/firestore';
import { Button } from '@material-ui/core';

const dbInstance = collection(database, 'notes');

function FromComponent({handleClose}) {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  let timeOut;
  const saveNote = () => {
    if(email || phone) {
      addDoc(dbInstance, { name: name, email: email, phone: phone, time: new Date().getTime() })
        .then(() => {
          clearState();
        })
    }
    return
  }

  const clearState = () => {
    handleClose && handleClose();
    clearTimeout(timeOut);
    timeOut = setTimeout(() => {
      setName('');
      setEmail('');
      setPhone('');
    }, 0);
  }

  const _disabled = email || phone;

  return (
    <div className='input-form'>
      <input
        className='input-item'
        placeholder='họ tên'
        onChange={(e) => setName(e.target.value)}
        value={name}
      />
      <input
        className='input-item'
        placeholder='số điện thoại'
        onChange={(e) => setPhone(e.target.value)}
        value={phone}
      />
      <input
        className='input-item'
        placeholder='email'
        onChange={(e) => setEmail(e.target.value)}
        value={email}
      />
      <Button
        color='primary'
        variant='contained'
        className='sign-up'
        fullWidth
        disabled={!_disabled}
        onClick={saveNote}
      >
        Đăng ký
      </Button>
    </div>
  );
}

export default  FromComponent;
