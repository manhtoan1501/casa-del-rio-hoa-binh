
import { Grid } from '@material-ui/core';

const LuxHome = () => {

  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12} sm={6}>
        <h1 className='title-base'>
          HƯƠNG VỊ THƯỢNG LƯU
        </h1>
        <p className='content-base'>
          Đến với dự án, quý khách hàng đến với những trải nghiệm nghỉ dưỡng hiện đại và tuyệt vời nhất giữa không gian xanh, cảnh sông nước yên bình và mát mẻ. Nếu thứ thu hút con người ta đến đây là cảnh quan thiên nhiên, sông suối giao hòa thì thứ níu giữ mỗi bước chân và để lại những khoảnh khắc khó quên hẳn là sự tiện nghi và hiện đại bậc nhất lại được tìm thấy ở nơi đây, mang đến những phút giây thư giãn trọn vẹn nhất.
        </p>
        <p className='content-base'>
          Với sự giao thoa của thiên nhiên, non nước các tiện ích được chủ đầu tư hình thành vô cùng xứng tầm với những trải nghiệm thượng lưu nhưng điều tiên quyết là vẫn không mất đi sự hài hòa với bức tranh mà dự án đang được đặt vào. Một số tiện ích nội khu nổi bật ngay trước thềm nhà, mang đến đa dạng các trải nghiệm cho quý khách hàng chỉ với một chạm tay.
        </p>
      </Grid>
      <Grid item xs={12} sm={6}>
        <img
          src={'https://casa-delrio.com/wp-content/uploads/2021/12/brochure-casa-final-view_compressed_page-0026-2048x1024.jpg'}
          className='image-lux-home'
        />
      </Grid>
    </Grid>
  );
}

export default LuxHome;
